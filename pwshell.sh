Param ($Usr, $PW)
$User = "$Usr"
$PWord = ConvertTo-SecureString -String "$PW" -AsPlainText -Force

$Credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $User, $PWord

$s = New-PSSession -ComputerName имя_пк -Credential:$Credential

echo "Checking diff content repository between git and the target repository."
$firstFolder = Invoke-Command -Session $s {Get-ChildItem -Recurse D:\gitlb-task\test-task\}
$secondFolder = Get-ChildItem -Recurse C:\Gitlab-Runner\builds\хз_что\0\12345hell\12345project
$diffFolder = Compare-Object -ReferenceObject $firstFolder -DifferenceObejct $secondFolder -Property Name, Length -PassThru

if ( %null -eq $diffFolder ) {
  echo "No chahge. Stop Pipeline"
  return
}
else {
  echo "New data. Archive old test-task in pipe-test repo"
  Invoke-Command -Session $s {Compress-Archive D:\gitlab-task\test-task -DestinationPath D:\gitlab-task\test-task_$($((get-date).ToString("yyyyMMdd-HHmmss"))) -CompressionLevel Optimal}

  echo "Copy test-task in pipe-test repo"
  Copy-Item -Path C:\Gitlab-Runner\builds\хз_что\0\FOR_TEST\12345hell\12345project -Destination D:\gitlab-task\ -Recurse -ToSession $s -Force
}

Remove-PSSession -ComputerName имя_пк


