#!/bin/bash

mapfile -t text < <(diff ~/test-dir/ ~/end-dir/ -rq)

if [ -z "${text[@]}" ]
  then
  echo 'No change. Stop pipeline'
else
  echo 'New data. Archive old test-task in pipe-test repo'
  tar -zcvf ~/end-dir_$(date "+%Y.%m.%d-%H.%M.%S").tar.gz -C ~ end-dir/

  for i in "${text[@]}"
    do
      if [[ $i =~ 'Only' ]]
        then
        echo $i | awk '{print$(NF-1)$NF}' | sed 's/:/\//g' | xargs -i cp -rv {} ~/end-dir/
      elif [[ $i =~ 'Files' ]]
        then
        echo $i | awk '{print$2}' | xargs -i cp -v {} ~/end-dir/
      fi
    done
fi




















